package com.devcamp.javabasic_j02.s10;

public class Account {
    String id;
    String name;
    int balance;

    public Account(String id, String name) {
        this.id = "00023";
        this.name = "NO";
    }

    public Account(String id, String name, int balance) {
        this.id = id;
        this.name = name;
        this.balance = balance;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getBalance() {
        return balance;
    }

    public int credit(int amount) {
        this.balance = amount;
        return balance;

    }

    public int debit(int amount) {
        if (amount <= balance) {
            balance = this.balance - amount;
        } else {
            System.out.println("Amount exceeded Balance");
        }
        return balance;

    }

    public int transferTo(Account another, int amount) {
        if (amount <= balance) {
            System.out.println("Transfer amount to the given account");
            balance = this.balance - amount;
        } else {
            System.out.println("Amount exceeded Balance");
        }
        return balance;

    }

    @Override
    public String toString() {
        return String.format("Account[Id = %s, name = %s, balance = %s", id, name, balance);

    }
}